﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace mangukool
{
    class Teacher : Inimene
    {
        static string Filename = @"..\..\Andmed\teachers.txt";

        public static IEnumerable<Teacher> Teachers => Nimekiri.OfType<Teacher>();
        public string Subject;

        public Teacher(string name, string id) : base(id, name)
        { }
        static Teacher() => LoeFailist();

        public static void LoeFailist() =>
                File.ReadAllLines(Filename)
         .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
             .ToList()
             .ForEach(x => new Teacher(x[0], x[2]).Subject= x[1]);
        //public Dictionary<string, Teacher> LoeFailist()
        //{
        //    var teachers = new Dictionary<string, Teacher>();
        //    string[] loetudread = File.ReadAllLines(Filename);
        //    foreach (var rida in loetudread)
        //    {
        //        string[] osad = rida.Split(',');
        //        string name = osad[0].Trim();
        //        string subject = osad[1].Trim();
        //        string id = osad[2].Trim();

        //        if (!teachers.Keys.Contains(id))
        //            teachers.Add(id, new Teacher
        //            {
        //                Name = name,
        //                Subject = subject,
        //                _Id = id
        //            });
        //    }
        //    return teachers;



        public static void WriteFail() =>
       File.WriteAllLines(Filename, Teachers.Select(x => $"{x.Id},{x.Name},{x.Subject}"));
       
 public override string ToString()
        {
            return $"Õpetaja {Name} õpetab {Subject}";
        }
    }
}



