﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace mangukool
{
    class Program
    {
        static void Main(string[] args)
        {
            Subject.ReadfromFile();
            foreach (var x in Subject.Subjects)

                Console.WriteLine(x);

            Inimene inimene = new Inimene("56324520", "Amelia");
            Inimene inimene1 = new Inimene("56658520", "Melissa");
            Inimene inimene2 = new Inimene("12324520", "Rauni");


            //Teacher t1 = new Teacher("Alisa", "54751133");
            //Console.WriteLine(t1);

            //foreach (var i in Teacher.Teachers)

            //    Console.WriteLine(i);

            // foreach (var y in Inimene.Teacher.Õpetajad)
            //Console.WriteLine(y);

            //katsetus teha uue inimese
            //Inimene inimene = new Inimene();
            //Console.WriteLine($"Alisa {inimene} 48404130100");
            // Õpilane kiir = new Õpilane { Nimi = "jorch adniel" };
            //Console.WriteLine(kiir);


            Parent p = new Parent("mingi isikukood", "Keegi");
            Console.WriteLine(p); // new Parent

            Student.LoeFailist();
            foreach (var x in Student.Students)
                Console.WriteLine(x); //loeme failist õpilasi

            Teacher.LoeFailist();
            foreach (var x in Teacher.Teachers)
                Console.WriteLine(x);  //loeme failist õpetajad


            Parent.LoeFailist();
            foreach (var x in Parent.Parents)
                Console.WriteLine(x);  //loeme failist vanemad

            foreach (var y in Parent.Parents)
                Console.WriteLine(y);


        }
    }
}
