﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace mangukool
{
    class Student : Inimene
    {
        static string Filename = @"..\..\Andmed\students.txt";
        public static IEnumerable<Student> Students => Nimekiri.OfType<Student>();
        public string Klass;

        
       // public string Klass => _Klass;

        private Student(string id, string name, string klass):base(id, name) { }
        static Student() => LoeFailist();

        public Student(string id, string name) : base(id, name)
        {
        }

        public static void LoeFailist()
            =>
            File.ReadAllLines(Filename)
            .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
            .ToList()
            .ForEach(x => new Student(x[0], x[1]).Klass=x[2]);

        static void WriteFail() =>
            File.WriteAllLines(Filename, Students.Select(x => $"{x.Id},{x.Name},{x.Klass}"));
        public override string ToString()
        {
            return $"{Klass} {Name} isikukoodiga {Id} õpib {Klass} klassis";
        }
       
       

}
}
