﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace mangukool
{

   

    class Subject
    {
        static string filename = @"..\..\Andmed\subjects.txt";

        public static Dictionary<string, Subject> Subjects =
            new Dictionary<string, Subject>();
      
        private string _Code; public string Code => _Code;
        private string _Title;
        public string Title
        {
            get => _Title;
            set
            {
                if (value != "")
                {
                    if (value != _Title)
                    {
                        _Title = value;
                        //KirjutaFaili();
                    }
}
            }
        }
        private string _Klass; public string Klass => _Klass; 
        private Subject(string code, string title, string klass)
        {
       
            _Code = code;
            _Title = title;
            _Klass = klass;

            Subjects.Add(code, this);
        }
        public static void ReadfromFile()
        {
            string[] loetudread = File.ReadAllLines(filename);
            foreach(var rida in loetudread)
            {
                string[] parts = rida.Split(',');
                string codex = parts[0].Trim();
                string title = parts[1].Trim();
                string klass = parts[2].Trim();
                if (!Subject.Subjects.Keys.Contains(codex))
                {
                    new Subject(codex, title, klass);
                }

            }
        }
        public static bool AddSubject (string code, string title, string klass)
        {
            if (Subjects.Keys.Contains(code)) return false;
            new Subject(code, title, klass);

            return true;
            
        }
        public override string ToString()
        {
            return $" {Title} õpetatakse {Klass} klassis";
        }


        /*static string Filename = @"..\..\andmed\õppeained.txt";
        static Dictionary<string, Õppeaine> Õppeained = LoeÕppeained();
      
        public static List<Õppeaine> Ained { get => Õppeained.Values.ToList(); }

        string _Kood;     
        string _Nimetus;

        public string Kood { get => _Kood; }   // ja neile vastavad getiga properid
        public string Nimetus { get => _Nimetus; }

       
        

    
               static Dictionary<string, Õppeaine> LoeÕppeained()
        {
            string[] ained = File.ReadAllLines(Filename);
            Dictionary<string, Õppeaine> dained = new Dictionary<string, Õppeaine>();
            foreach (var rida in ained)
            {
                string kood = rida.Split(',')[0].Trim();
                string nimetus = rida.Split(',')[1].Trim();
                if (!dained.Keys.Contains(kood)) dained.Add(kood, new Õppeaine { _Kood = kood, _Nimetus = nimetus });
            }
            return dained;

        }
        public static string AineNimi(string kood)
        {
            return Õppeained.Keys.Contains(kood) ? Õppeained[kood]._Nimetus : "";

        }
        */
        //class Program
        //{
        //static void Main(string[] args)
        //{

        //}

        //}


    }
}
