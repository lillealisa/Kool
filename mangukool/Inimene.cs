﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mangukool
{
   class Inimene
    {
        private static Dictionary<string, Inimene>
            _Inimesed = new Dictionary<string, Inimene>();
        public static List<Inimene> Nimekiri => _Inimesed.Values.ToList();

        public string Name;

        public string _Id;
        public string Id
        { get => _Id; }

        public Inimene (string id, string name)
        {
            _Id = id; Name = name;
            if (!_Inimesed.Keys.Contains(id))
                _Inimesed.Add(_Id, this);

        }


        // public Inimene(string nimi, string ik) { Nimi = nimi; I = ik;  }
        // kas on seda public Inimest vaja üldse?
        public override string ToString()
        {
            return $"{Name} isikukoodiga {Id} ";
        }


    }
}