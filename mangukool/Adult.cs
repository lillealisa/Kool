﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mangukool
{
    class Adult : Inimene

    {
        static string Filename = @"..\..\Andmed\adults.txt";

        Adult(string id, string name) : base (id, name) { }
        public static IEnumerable<Adult> Adults => Inimene.Nimekiri.OfType<Adult>();
        public static IEnumerable<Adult> Teachers => Inimene.Nimekiri.OfType<Adult>().Where(x => x.Subject != "");
        public static IEnumerable<Adult> Parents => Inimene.Nimekiri.OfType<Adult>().Where(x => x.KasParent);

        string _Subject = ""; public string Subject => _Subject;
        string _Klass = ""; public string Klass => _Klass;
        public bool KasTeacher => _Subject != "";

        public bool KasJuhataja => _Klass != "";

        List<Student> _Children = new List<Student>();
        public IEnumerable<Student> Children => _Children.AsEnumerable();
        public bool KasParent => _Children.Count > 0;
    }
}
